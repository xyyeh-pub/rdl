/*
 * Original Copyright (c) 2011-2016 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 *
 *
 * RDL - Robot Dynamics Library
 * Modifications Copyright (c) 2017 Jordan Lack <jlack1987@gmail.com>
 *
 * Licensed under the zlib license. See LICENSE for more details.
 */

#ifndef __RDL_URDFREADER_H__
#define __RDL_URDFREADER_H__

#include "rdl_dynamics/types.hpp"

namespace RobotDynamics
{
struct Model;
namespace Urdf
{
/**
 * @brief Read urdf from file path
 * @param filename
 * @param model
 * @param floating_base
 * @param verbose
 * @return
 */
bool urdfReadFromFile(const char* filename, ModelPtr model, bool floating_base, bool verbose = false);

/**
 * @brief Read urdf from file path
 * @param filename
 * @param model
 * @param floating_base
 * @param verbose
 * @return
 */
bool urdfReadFromFile(const std::string& filename, ModelPtr model);

/**
 * @brief Read urdf from string contents
 * @param model_xml_string
 * @param model
 * @param floating_base
 * @param verbose
 * @return
 */
bool urdfReadFromString(const char* model_xml_string, ModelPtr model, bool floating_base, bool verbose = false);

/**
 * @brief Read urdf from string contents
 * @param model_xml_string
 * @param model
 * @param verbose
 * @return
 *
 * @note This function will deduce whether or not the robot has a floating base by checking the name of
 * the root link. If the name of the root link is "world", then it will have a floating base.
 */
bool urdfReadFromString(const std::string& model_xml_string, ModelPtr model);

/**
 * @brief This will build a map of joint name to body name.
 * @param model_xml_string Urdf file contents
 * @param jointBodyMap Modified
 * @return
 *
 * @warning This will NOT give any information about a floating body/joint. The floating body will be
 * ignored since it's not moved by a joint called out in the urdf. Only joints/bodies in 'joint'/'link'
 * tags will be used to populate the map.
 */
bool parseJointBodyNameMapFromString(const char* model_xml_string, std::map<std::string, std::string>& jointBodyMap);

/**
 * @brief This will build a map of joint name to body name.
 * @param model_xml_string Urdf file contents
 * @param jointBodyMap Modified
 * @return
 *
 * @warning This will NOT give any information about a floating body/joint. The floating body will be
 * ignored since it's not moved by a joint called out in the urdf. Only joints/bodies in 'joint'/'link'
 * tags will be used to populate the map.
 */
bool parseJointBodyNameMapFromString(const std::string& model_xml_string, std::map<std::string, std::string>& jointBodyMap);

/**
 * @brief This will build a map of joint name to body name.
 * @param filename Filepath
 * @param jointBodyMap Modified
 * @return
 *
 * @warning This will NOT give any information about a floating body/joint. The floating body will be
 * ignored since it's not moved by a joint called out in the urdf. Only joints/bodies in 'joint'/'link'
 * tags will be used to populate the map.
 */
bool parseJointBodyNameMapFromFile(const char* filename, std::map<std::string, std::string>& jointBodyMap);
}  // namespace Urdf
}  // namespace RobotDynamics

/* ___RDL_URDFREADER_H__ */
#endif  // ifndef __RDL_URDFREADER_H__
