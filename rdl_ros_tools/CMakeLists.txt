cmake_minimum_required(VERSION 3.4)
project(rdl_ros_tools)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

find_package(catkin REQUIRED COMPONENTS rdl_cmake rdl_msgs rdl_dynamics rdl_urdfreader roscpp sensor_msgs geometry_msgs nav_msgs)

catkin_package(
 CATKIN_DEPENDS rdl_msgs rdl_dynamics rdl_urdfreader roscpp sensor_msgs geometry_msgs nav_msgs
)

include_directories(SYSTEM ${catkin_INCLUDE_DIRS})

add_executable(rdl_kinematics_interface src/kinematics_interface.cpp)
target_link_libraries(rdl_kinematics_interface ${catkin_LIBRARIES})
add_dependencies(rdl_kinematics_interface ${catkin_EXPORTED_TARGETS} ${rdl_msgs_EXPORTED_TARGETS})

install(
  TARGETS 
    rdl_kinematics_interface
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

include(Format)
format_code_target()

if(CATKIN_ENABLE_TESTING)
    find_package(rostest REQUIRED)

    add_rostest_gtest(${PROJECT_NAME}_kinematics_interface_test
            test/kinematics_interface/kinematics_interface.test
            test/kinematics_interface/kinematics_interface_test.cpp)
    target_link_libraries(${PROJECT_NAME}_kinematics_interface_test ${catkin_LIBRARIES})

    include(CodeCoverage)
    coverage_add_target(run_tests_${PROJECT_NAME})

    file(GLOB_RECURSE cpp_files ${PROJECT_SOURCE_DIR}/src/*.c* ${PROJECT_SOURCE_DIR}/include/*/*.h*)

    include(Lint)
    lint_library(INCLUDE_DIRS include/${PROJECT_NAME} SRCS ${cpp_files})
endif()